import requests
import bs4
import pandas as pd
from datetime import datetime


#PageURL = "https://hk.jobsdb.com/hk/search-jobs/data-engineer/1"

list_title = []
list_company = []
list_location = []
list_url = []


def GetData(url):

  r = requests.get(url)
  if r.status_code == requests.codes.ok:
    print(f"Success Access {url}\n")
    data = r.text
  # print(data)


  data = bs4.BeautifulSoup(r.content, "html.parser")
  print(F'Website Title: {data.title.string}\n')
  #print(root.prettify())


  jobs = data.find_all("div", class_ = "sx2jih0 zcydq85k zcydq84t zcydq83t zcydq842")
  #print(jobs[1])

  #titles = data.find_all("h1", class_ = "FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 sQuda_0")
  titles = data.find_all("h1", class_ = "sx2jih0 zcydq82b _18qlyvc0 _18qlyvcv _18qlyvc3 _18qlyvc8")
  #print(titles)
  for title in titles:
    list_title.append(title.text)


  a = jobs[0].find("h1", class_ = "sx2jih0 zcydq82b _18qlyvc0 _18qlyvcv _18qlyvc3 _18qlyvc8")
  print(a.text)


  #companys = data.find("div", class_="FYwKg _36UVG_0").find_all("span", class_ = "FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0")
  companys = data.find("div", class_ = "sx2jih0 zcydq878").find_all("span", class_ = "sx2jih0 zcydq82b _18qlyvc0 _18qlyvcv _18qlyvc1 _18qlyvc8")
  # #print(companys)
  for company in companys:
    list_company.append(company.text)


  b = jobs[0].find("span", class_ = "sx2jih0 zcydq82b _18qlyvc0 _18qlyvcv _18qlyvc1 _18qlyvc8")
  print(b.text)

  #locations = data.find("div", class_ = "FYwKg _36UVG_0").find_all("span", class_ = "FYwKg _3MPd_ _2Bz3E And8z")
  locations = data.find("div", class_ = "sx2jih0 zcydq878").find_all("span", class_ = "sx2jih0 zcydq82b zcydq8r iwjz4h0")
  #print(locations)
  for location in locations:
    list_location.append(location.text)

  try:
    c = jobs[0].find("span", class_ = "sx2jih0 zcydq82b zcydq8r iwjz4h0")
    print(c.text)
  except:
    print("--")

  #Nextpage = data.find("a", class_ = "FYwKg _2MJ7O_0 _28IEJ_0 _2ZHQ__0 _2rhiT_0 _2Nj6k FLByR_0 _2QIfI_0 _3ftyQ _3O7rA _1ouuf_0")
  Nextpage = data.find("a", class_ = "sx2jih0 zcydq85e zcydq84n zcydq88 zcydq81x zcydq82k zcydq898 zcydq81q zcydq827 zcydq826 _1ouuf_0")
  return(Nextpage["href"])


def GetData_2(url, count):
  """
  get data from url in function loop_getdata,
  get count(page number) in in function loop_getdata
  """
  r = requests.get(url)
  if r.status_code == requests.codes.ok:
    print(f"Success Access: {url}")
    data = r.text
  else:
    print("Access Declined")

  data = bs4.BeautifulSoup(r.content, "html.parser")
  #print(F'Website Title: {data.title.string}')
  #print(root.prettify())

  count_jobs = 0
  try:
    while count_jobs <= 999999999: #<<<How many Jobs in one page. Should be 30, but use 999999999 to try. 
      jobs = data.find_all("div", class_ = "sx2jih0 zcydq85k zcydq84t zcydq83t zcydq842")
      job = jobs[count_jobs]
      #print(jobs[1])

      try:
        title = job.find("h1", class_ = "sx2jih0 zcydq82b _18qlyvc0 _18qlyvcv _18qlyvc3 _18qlyvc8")
        list_title.append(title.text)
      except:
        list_title.append("--")

      try:
        compay = job.find("span", class_ = "sx2jih0 zcydq82b _18qlyvc0 _18qlyvcv _18qlyvc1 _18qlyvc8")
        list_company.append(compay.text)
      except:
        #Company Confidential
        list_company.append("--")

      try:
        location = job.find("span", class_ = "sx2jih0 zcydq82b zcydq8r iwjz4h0")
        list_location.append(location.text)
      except:
        #No Location
        list_location.append("--")

      try:
        job_url = job.find("a", class_ = "DvvsL_0 _1p9OP")
        final_job_url = "https://hk.jobsdb.com"+ job_url["href"]
        list_url.append(final_job_url)
      except:
        #No url
        list_url.append("--")
        print("--")

      count_jobs += 1

  except:
    print(f"Scraping Page {count} successfully!")


  Nextpage = data.find("a", class_ = "sx2jih0 zcydq85e zcydq84n zcydq88 zcydq81x zcydq82k zcydq898 zcydq81q zcydq827 zcydq826 _1ouuf_0")
  return(Nextpage["href"])



def loop_getdata():
  """
  ask user kerword and pages
  then use function to get data
  """
  global Job_keyword
  global Pages
  Job_keyword = input("Input Job Keyword (Default is data-engineer): ") or "data-engineer"
  Pages = int(input("How Many Pages You Want To Search? "))
  PageURL = f"https://hk.jobsdb.com/hk/search-jobs/{Job_keyword}/"
  print(" ")

  
  count = 1
  while count <= Pages:
    PageURL2 = PageURL + str(count)
    GetData_2(PageURL2,count)
    count += 1



def data_to_df():
  df = pd.DataFrame(
      {'Job Title': list_title,
      'Company': list_company,
      'Location': list_location,
      'url': list_url
      })

  print(df)

  df.to_csv(f'JobsDB_{Job_keyword}_{Pages}Pages.csv',encoding="utf-8-sig", index=False)
  print("Save To CSV Successfully!")



def main():
  start_time = datetime.now()
  print("Start time: ", start_time)

  loop_getdata()
  data_to_df()

  end_time = datetime.now()
  print("End time: ", end_time)
  process_time = end_time - start_time
  print("Process time: ", process_time)



if __name__ == '__main__':

    main()